/**
 * Created by cl-macmini-48 on 8/20/16.
 */
'use strict';

exports.connectSocket = function (server) {
    if (!server.app) {
        server.app = {}
    }
    server.app.socketConnections = {};
    let nicknames = {};
    let socket = require('socket.io').listen(server.listener);
    //socket.set('log level', 2);
    //socket.set('transports', [ 'websocket', 'xhr-polling' ]);
    socket.on('disconnect', function(){
        console.log('socket disconnected')
    });
    // socket.sockets.on('connection', function (socket) {
    //     socket.on('user message', function (msg) {
    //         console.log("socket");
    //         socket.broadcast.emit('user message', socket.nickname, msg);
    //         socket.on('nickname', function (nick, fn) {
    //             if (nicknames[nick]) {
    //                 console.log(true);
    //             } else {
    //                 console.log(false);
    //                 nicknames[nick] = socket.nickname = nick;
    //                 socket.broadcast.emit('announcement', nick + ' connected');
    //                 io.sockets.emit('nicknames', nicknames);
    //             }
    //         });
    //
    //         socket.on('disconnect', function () {
    //             if (!socket.nickname) return;
    //
    //             delete nicknames[socket.nickname];
    //             socket.broadcast.emit('announcement', socket.nickname + ' disconnected');
    //             socket.broadcast.emit('nicknames', nicknames);
    //         });
    //     });
    // });
    socket.sockets.on('connection', function(socket){
        console.log('connection');
        server.app.socketConnections['12345678'] = {
            socketId: socket.id
        };
        socket.on('messageFromCustomer', function (data) {
            console.log("234342342423",data);
            //Update SocketConnections
            if (data || data.customer) {   //&&
               // TokenManager.decodeToken(data.token, function (err, decodedData) {
                   // if (!err && decodedData.id) {
                        //  console.log('decoded token id',decodedData.id)
                        if (server.app.socketConnections.hasOwnProperty(data.customer.id)) {
                            server.app.socketConnections[data.customer.id].socketId = socket;
                            var sparkIdToSend = server.app.socketConnections[data.admin.id || null]
                                && server.app.socketConnections[data.admin.id.toString() || null].socketId;
                            //console.log("herer Id of admin", sparkIdToSend);
                            socket.broadcast.emit('messageFromCustomerToAdmin', {
                                message: data.message,
                                eventType: 'To All Admins',
                                data : {faddo: 'good'},
                                performAction : 'INFO'
                            });
                            socket.emit('messageFromCustomerToAdmin', {
                                message: data.message,
                                eventType: 'To All Admins',
                                data : {faddo: 'good'},
                                performAction : 'INFO'
                            });
                        } else {
                            server.app.socketConnections[data.customer.id] = {
                                socketId: socket
                            };
                            //console.log('here SocketID', server.app.socketConnections[data.customer.id].socketId);
                            var sparkIdToSend = server.app.socketConnections[data.admin.id || null]
                                && server.app.socketConnections[data.admin.id.toString() || null].socketId;

                            // socket.to(sparkIdToSend).emit('messageFromCustomerToAdmin', {
                            //     message:'Socket id Updated',eventType:'INFO'
                            // });
                            socket.broadcast.emit('messageFromCustomerToAdmin', {
                                message: data.message,
                                eventType: 'To All Admins',
                                data : {faddo: 'good'},
                                performAction : 'INFO'
                            });
                            socket.emit('messageFromCustomerToAdmin', {
                                message: data.message,
                                eventType: 'To All Admins',
                                data : {faddo: 'good'},
                                performAction : 'INFO'
                            });
                        }
                       // BookingController.socketSendToFailed(data.token);

                    //} else {
                   //     socket.emit('messageFromServer', { message:'Invalid Token',eventType:'INFO'});
                   // }
              //  })
            } else {
                console.log('msgFromClient',data)
            }
        });
        socket.on('messageFromContrator', function (data) {
            console.log("234342342423",data);
            //Update SocketConnections
            if (data || data.contractor) {   //&&
                // TokenManager.decodeToken(data.token, function (err, decodedData) {
                // if (!err && decodedData.id) {
                //  console.log('decoded token id',decodedData.id)
                if (server.app.socketConnections.hasOwnProperty(data.contractor.id)) {
                    server.app.socketConnections[data.contractor.id].socketId = socket;
                    var sparkIdToSend = server.app.socketConnections[data.admin.id || null]
                        && server.app.socketConnections[data.admin.id.toString() || null].socketId;
                    socket.broadcast.emit('messageFromContractorToAdmin', {
                        message: data.message,
                        eventType: 'To all admin from contractor',
                        data : {faddo: 'good'},
                        performAction : 'INFO'
                    });
                    socket.emit('messageFromContractorToAdmin', {
                        message: data.message,
                        eventType: 'To all admin from contractor',
                        data : {faddo: 'good'},
                        performAction : 'INFO'
                    });
                } else {
                    server.app.socketConnections[data.contractor.id] = {
                        socketId: socket
                    };
                   // console.log('here SocketID', server.app.socketConnections[data.contractor.id].socketId);
                    var sparkIdToSend = server.app.socketConnections[data.admin.id || null]
                        && server.app.socketConnections[data.admin.id.toString() || null].socketId;

                    socket.broadcast.emit('messageFromContractorToAdmin', {
                        message: data.message,
                        eventType: 'To all admin from contractor',
                        data : {faddo: 'good'},
                        performAction : 'INFO'
                    });
                    socket.emit('messageFromContractorToAdmin', {
                        message: data.message,
                        eventType: 'To all admin from contractor',
                        data : {faddo: 'good'},
                        performAction : 'INFO'
                    });
                }
                // BookingController.socketSendToFailed(data.token);

                //} else {
                //     socket.emit('messageFromServer', { message:'Invalid Token',eventType:'INFO'});
                // }
                //  })
            } else {
                console.log('msgFromClient',data)
            }
        });
       socket.on("messageFromAdmin", function (data) {
           console.log('here data',data);
           if (data && data.customer) {
               if (server.app.socketConnections.hasOwnProperty(data.customer.id)) {
                   server.app.socketConnections[data.customer.id].socketId = socket;
                   //console.log('To Customer',server.app.socketConnections);
                   var sparkIdToSend = server.app.socketConnections[data.customer.id.toString() || null]
                       && server.app.socketConnections[data.customer.id.toString() || null].socketId;
                  // console.log('Customer',sparkIdToSend);
                   server.app.socketConnections[data.customer.id.toString() || null].socketId.emit('messageFromAdminToCustomer', { message: data.message,eventType: 'From admin'});
                   //socket.to(sparkIdToSend).emit('messageFromAdminToCustomer', { message: data.message,eventType: 'From admin'});
               } else {
                   server.app.socketConnections[data.customer.id] = {
                       socketId: socket
                   };
                  // console.log('To Customer new',server.app.socketConnections);
                  // console.log('Customer',sparkIdToSend);
                   var sparkIdToSend = server.app.socketConnections[data.customer.id.toString() || null]
                       && server.app.socketConnections[data.customer.id.toString() || null].socketId;
                   server.app.socketConnections[data.customer.id.toString() || null].socketId.emit('messageFromAdminToCustomer', { message: data.message,eventType: 'From admin'});

                   //socket.to(sparkIdToSend).emit('messageFromAdminToCustomer', { message:data.message,eventType: 'From admin'});
               }
           } else if (data && data.contractor){
               if (server.app.socketConnections.hasOwnProperty(data.contractor.id)) {
                   server.app.socketConnections[data.contractor.id].socketId = socket;
                   var sparkIdToSend = server.app.socketConnections[data.contractor.id.toString() || null]
                       && server.app.socketConnections[data.contractor.id.toString() || null].socketId;
                  // console.log('Contractor',sparkIdToSend);
                  // console.log('To Contractor',server.app.socketConnections);
                   server.app.socketConnections[data.contractor.id].socketId.emit('messageFromAdminToContractor', { message:data.message, eventType:'From admin'});
                  // socket.to(sparkIdToSend).emit('messageFromAdminToContractor', { message:data.message, eventType:'From admin'});
               } else {
                   server.app.socketConnections[data.contractor.id] = {
                       socketId: socket
                   };
                 //  console.log('To Contractor new',server.app.socketConnections);
                  // console.log('Contractor',sparkIdToSend);
                   var sparkIdToSend = server.app.socketConnections[data.contractor.id.toString() || null]
                       && server.app.socketConnections[data.contractor.id.toString() || null].socketId;
                   console.log('To Contractor',server.app.socketConnections);
                   server.app.socketConnections[data.contractor.id.toString() || null].socketId.emit('messageFromAdminToContractor', { message:data.message, eventType:'From admin'});
                   //socket.to(sparkIdToSend).emit('messageFromAdminToContractor', { message:data.message, eventType:'From admin'});
               }
               //socket[].emit('messageFromAdminToContractor', {message: 'WELCOME TO CHATBOT', eventType: 'CHAT', data: data});
           }

       })

    });

    //Customer Notifications :

    //Bookings
    process.on('sendPushToContractor', function (data) {
        if (data.contractorId){
            var sparkIdToSend = server.app.socketConnections[data.contractorId.toString() || null]
                && server.app.socketConnections[data.contractorId.toString() || null].socketId;
            //GetsocketId
            if (sparkIdToSend) {
                console.log('send request to', data.contractorId)
                socket.to(sparkIdToSend).emit('messageFromServer', {
                    message: data.messageToDisplay,
                    eventType: data.eventType,
                    data : {bookingData : data.bookingData},
                    performAction : 'INFO'
                });
            } else {
                console.log('Socket id not found for ' + data.contractorId)
            }
        }else {
            console.log('contractorId id not given')
        }
    });

    process.on('accountApprovedByAdmin', function (data) {
        if (data.userId){
            var sparkIdToSend = server.app.socketConnections[data.userId.toString() || null]
                && server.app.socketConnections[data.userId.toString() || null].socketId;
            //GetsocketId
            if (sparkIdToSend) {
                socket.to(sparkIdToSend).emit('messageFromServer', {
                    message: data.messageToDisplay,
                    eventType: data.eventType,
                    data : null,
                    performAction : 'INFO'
                });
            } else {
                console.log('Socket id not found for ' + data.customerId)
            }
        }else {
            console.log('userId id not given')
        }
    });
    // Customer Sockets
    //Common Push Sender Function
    function sendPushForCustomer(data){
        if (data.customerId){
            var sparkIdToSend = server.app.socketConnections[data.customerId || null]
                && server.app.socketConnections[data.customerId.toString() || null].socketId;
            //GetsocketId
            if (sparkIdToSend) {
                socket.to(sparkIdToSend).emit('messageFromServer', {
                    message: data.messageToDisplay,
                    eventType: data.eventType,
                    data : {bookingData : data.bookingData},
                    performAction : 'INFO'
                });
            } else {
                console.log('Socket id not found for ' + data.customerId)
            }
        }else {
            console.log('customerId id not given')
        }
    }

    //Booking Related Event Listeners as follows :

    //let eventArray = [
    //    'bookingAcceptedEvent',
    //    'bookingCancelByContractorEvent',
    //    'bookingArrivedDestinationEvent',
    //    'bookingCompletedByContractorEvent',
    //    'paymentFailedForBookingEvent'
    //];
    //eventArray.forEach(function (eventName) {
    //    process.on(eventName, function (data) {
    //        sendPushForCustomer(data)
    //    });
    //})


};

