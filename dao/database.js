/**
 * Created by Shashank on 12-12-2015.
 */
var Mongoose = require('mongoose'),
    config = require('../config/serverPort'); // includes file which has app configuration details such as hostname, hostport, etc ..
const response=require('../constant/response');
Mongoose.connect('mongodb://' + config.DataBaseSetting.host + ':' + '/' + config.DataBaseSetting.db);
var db = Mongoose.connection;
db.on('error', console.error.bind(console, response.connectionError));
db.once('open', function callback() {
    console.log("Connection with database succeeded.");
});

exports.Mongoose = Mongoose;
exports.db = db;
