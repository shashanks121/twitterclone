/**
 * Created by Shashank on 14-12-2015.
 */
'use strict';
var  Model = require('../model/index'),
    encrypt=require('../controllers/encrypt'),
 sendResponse=require('../constant/sendResponse');
//var ObjectId = require('mongodb').ObjectID;
exports.saveData=function(userData,reply){
    userData.accessToken=encrypt.accessToken(userData);
    encrypt.password(userData.password,function(encryptedPassword){
        userData.password=encryptedPassword;
        let user = new Model.User(userData);
        user.save(function(err, docs ){
            if(err){
                console.log(err);
                sendResponse.somethingWentWrongError(reply);
            }
            else {
                console.log("user saved succefully");
                sendResponse.successMsgToken(user.accessToken, reply);
                console.log("Docsssf", docs);
            }
        });
    });
};
exports.findData=function(data, callback){
    Model.User.find({"email":data},function(err,docs){
       if(err){
           console.log(err);
       }else{
        callback(docs);
       }
        }
    ).limit(1);
};
exports.updateToken=function(data,reply){
    Model.User.findOne({email:data.email},function(err,docs){
       if(err){
           console.log(err);
       }else{
           console.log(docs)
           docs.accessToken=encrypt.accessToken(data);
           docs.visits++;
           docs.save();
           sendResponse.successMsgToken(docs.accessToken,reply);
       }
    }).limit(1);
}
exports.findUsers=function(searchQuery,reply){

    let Query = Model.User.find({ 'firstName':  searchQuery },{ firstName: 1, email: 1 , followTable: 1},{})
        .populate({path : "followTable", select: " followingNames follower follows followerNames followingNames" })
        .exec(function (err, someValue) {
        if (err) console.log(err);
            console.log(someValue);
        sendResponse.successData(someValue[0], reply);
    });
};
exports.findById=function(ID, callback){
    let projection = {
        __v: 0,
        accessToken: 0,
        password: 0,
        updatedAt: 0,
        createdAt: 0,
        followTable: 0
    };

    Model.User.find({"_id": ID}, projection,function (err, docs) {
        if (err) {
            console.log(err);
            return callback(err);
        } else if (docs == null) {
            return callback('err');
        }
        callback(null, docs[0])
    }
    ).limit(1);
    
};

exports.followUser = function(format, userInfo, callback) {
   
    var update = { $inc: { follower: 1 }, $push: { followerNames: userInfo[1] } };
    var options = {new: true ,safe: true, upsert: true};
    Model.Follower.findOneAndUpdate(format, update, options,function(err, docs) {
        if (err) {
            console.log(err);
            callback(err);
        } else if (docs == null) {

        }
        userInfo.pop();
        callback();
        var userFormat = {
            "_id": docs.Id
        }
        var userUpdate = {$set: {followTable: docs._id}  };
        Model.User.findOneAndUpdate( userFormat, userUpdate,function(err, docs) {
            if(err) {
                console.log("Idhar",err)
            }

        });
    });
};

exports.userFollowing = function(format, userInfo, callback) {
  

    var update = { $inc: { follows: 1 }, $addToSet: { followingNames: userInfo[0] } };
    var options = {new: true ,safe: true, upsert: true};
    Model.Follower.findOneAndUpdate(format, update, options,function(err, docs) {
        if (err) {
            console.log(err);
            callback(err);
        } else if (docs == null) {
           console.log("YAHAN")
        }
        userInfo.pop();
        callback();
        var userFormat = {
            "_id": docs.Id
        }
        var userUpdate = {$set: {followTable: docs._id}  };
        Model.User.findOneAndUpdate( userFormat, userUpdate,function(err, docs) {
            if(err) {
                console.log("Idhar",err)
            }
            
        });
    });
};
