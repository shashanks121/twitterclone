/**
 * Created by Shashank on 11-12-2015.
 */
 'use strict';
var async = require('async'),
    Joi=require('joi'),
    Query = require('../dao/query'),
    controllers=require('../controllers/index'),
    sendResponse = require('../constant/sendResponse');
const response = require('../constant/response');
module.exports = [{
    method: 'POST',
    path: '/api/user/signUp/',
    config:{
        handler:function (request, reply){
            var userData=request.payload;
             async.waterfall([
                function(callback){
                 controllers.valid.checkParameter(userData,reply,callback);
                },
                function(callback){
                  controllers.valid.checkEmailAvailability(userData,callback);
                }],function(err){
                if(err){
                    return sendResponse.emailExistError(reply);
                }else {
                    //userData.visits = 0;
                    Query.saveData(userData, reply);
                }
            });
        },
        description: 'Add User',
        notes: ['Adds a User to twitter account'],
        plugins: {
            'hapi-swagger': {
                responseMessages: response.standardHTTPErrors,
                payloadType: 'form',
                nickname: 'new user '
            }
        },
        tags: ['api','signUp'],
        validate: {
            payload: {
                firstName: Joi.string().min(3).max(30)
                    .required()
                    .description('first Name'),
                lastName: Joi.string().min(3).max(30)
                    .required()
                    .description('last Name'),
                email: Joi.string()
                    .email()
                    .required().description('email of user'),
                password: Joi.string().alphanum().min(8).max(16)
                    .required()
                    .description('password of user')
            }
        }
    }
},{
    method: 'POST',
    path: '/api/user/login/',
    config:{
    handler:function (request, reply) {
        var userData=request.payload;
        async.waterfall([
                function(callback){
                    controllers.valid.checkParameter(userData,reply,callback);
                },
                function(callback){
                    controllers.valid.checkEmailandPassword(userData,callback);
                }
            ],
            function(err){
                if(err){
                    return sendResponse.invalidInput(reply);
                }
                else{
                    Query.updateToken(userData, reply);
                }
            });
    },
    description: 'login User',
        notes: ['login a User to twitter account'],
        plugins: {
        'hapi-swagger': {
            responseMessages: response.standardHTTPErrors,
                payloadType: 'form',
                nickname: 'twtrUser'
        }
    },
    tags: ['api','login'],
        validate: {
        payload: {
            email: Joi.string()
                .email()
                .required().description('email of user'),
                password: Joi.string().alphanum().min(8).max(16)
                .required()
                .description('password of user')
        }
    }
}
},{
    method: 'POST',
    path: '/api/user/search/',
    config:{
        auth:{
            strategy:'accessToken'
        },
        handler:function (request, reply) {
            var userData=request.payload;
            async.waterfall([
                    function(callback){
                        controllers.valid.checkParameter(userData,reply,callback);
                    },
                ],
                function(err){
                    if(err){
                        return sendResponse.invalidInput(reply);
                    }
                    else{
                        Query.findUsers(userData.search, reply);
                    }
                });
        },
        description: 'search User',
        notes: ['search a User of twitter account'],
        plugins: {
            'hapi-swagger': {
                responseMessages: response.standardHTTPErrors,
                payloadType: 'json',
                nickname: 'twtrUser'
            }
        },
        tags: ['api','login'],
        validate: {
            payload: {
                search: Joi.string().alphanum().required()
                    .description('serching of twitter user')
            },
            headers:Joi.object({
                    authorization: Joi.string()
                        .default('Bearer ')
                        .description('bearer token')
                }).unknown()
        }
    }
},{
    method: 'PUT',
    path: '/api/user/follow/{userId}/{followerId}',
    config:{
        auth:{
            strategy:'accessToken'
        },
        handler:function (request, reply) {
            var userID=request.params.userId;
            var followerID=request.params.followerId;
            var arrayResult = [];
            async.waterfall([
                    function(callback){
                        controllers.valid.checkId(userID, arrayResult, callback);
                    },
                    function(callback){
                        controllers.valid.checkId(followerID, arrayResult, callback);
                    },
                    function( callback){
                        controllers.follows.follower(followerID, arrayResult, callback);
                    },
                    function (callback) {
                        controllers.follows.following(userID, arrayResult, callback);
                    }
                ],
                function(err,result){
                    if(err){
                        return sendResponse.invalidInput(reply);
                    }
                    else{
                        var data ={
                            result: result 
                        };
                        return sendResponse.successData(data, reply);
                    }
                });
        },
        description: 'follow User',
        notes: ['follow a User of twitter account'],
        plugins: {
            'hapi-swagger': {
                responseMessages: response.standardHTTPErrors,
                nickname: 'twtrUser'
            }
        },
        tags: ['api','login'],
        validate:{
            params: {
                userId: Joi.string().required()
                    .description('user Id of user'),
                followerId: Joi.string().required()
                    .description('follower Id of user')
            },
            headers:Joi.object({
                authorization: Joi.string()
                    .default('Bearer ')
                    .description('bearer token')
            }).unknown()
        }
    }
}
];
