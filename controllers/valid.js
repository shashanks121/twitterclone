/**
 * Created by Shashank on 18-12-2015.
 */
'use Strict';
var Query = require('../dao/query'),
    encrypt = require('./encrypt'),
  sendResponse = require('../constant/sendResponse');
exports.checkParameter=function(userData,reply,callback){
    for( key in userData ) {
        if(userData[key] == '(null)'||userData[key] == undefined||userData[key] == ''||userData[key] == 0||userData[key] == false){
            console.log('check');
             sendResponse.parameterMissingError(reply);
             break;
        }
        else{
            callback(null);
        }
    }
};
exports.checkEmailAvailability=function(userData,callback){
   var regexpOfEmail='^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$';
    if(userData.email.match(regexpOfEmail)){
        Query.findData(userData.email,function(result){
           if(result==''){
               callback(null);
           }
           else{
               callback('err');
           }
        });
    }
   else{
        callback('err');
}
};
exports.checkEmailandPassword=function(userdata,callback){
    Query.findData(userdata.email,function(user){
        if(user==''){
            callback('err');
        }
        else{
            encrypt.compare(userdata.password, user[0].password, function(result){
             if(result==true){
                callback(null);
             }
             else{
                callback('err');
             }
            });
        }
    });
};
exports.checkId=function(userId, results, callback){
    
    if(typeof results == 'undefined') {
        results = [];
    }
    Query.findById(userId, function (err, data) {
            if (err) {
                callback('err');
            }
            else {
                console.log(typeof results);
                results.push(data);
                callback();
            }
        });
}