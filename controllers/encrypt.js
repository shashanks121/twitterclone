/**
 * Created by Shashank on 18-12-2015.
 */
"use strict";
var jwt = require('jsonwebtoken'),
    bcrypt=require('bcryptjs'),
    Query = require('../dao/query'),
    token=require('../config/token');
exports.accessToken=function(userData){
    var tokenBase={
        Base:userData.email
    };
    let accesstoken = jwt.sign(tokenBase, token.secretKey, token.algorithm );
    return accesstoken;
};
exports.validate = function (request, decodedToken, callback) {
    Query.findData(decodedToken.Base,function(result){
        var error,
            credentials={
                firstName:result[0].firstName,
                LastName:result[0].lastName,
                email: result[0].email,
                _id: result[0]._id
            };
        if (!result) {
            return callback(error, false, credentials);
        }
        else {
            return callback(error, true, credentials)
        }
    });
};

exports.password=function(password,callback){
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(password, salt, function(err, hash) {
            // Store hash in your password DB.
            callback(hash);
        });
    });
};
exports.compare=function(password,dbPassword,callback){
    bcrypt.compare(password, dbPassword, function(err, res) {
        if(err){
            throw err;
        }
        else{
        callback(res);
        }
    });
};