/**
 * Created by cl-macmini52 on 5/30/16.
 */
"use strict";
var async = require('async');
var Query = require('../dao/query');
exports.follower = function(data, userInfo, callback) {
    var userFormat = {
        "Id": data
    };
    
    Query.followUser(userFormat, userInfo, callback);  
  
};


exports.following = function(data, userInfo, callback) {
    var userFormat = {
        "Id": data
    };
   
    Query.userFollowing(userFormat,  userInfo, callback);
  
};