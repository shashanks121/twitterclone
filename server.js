/**
 * Created by Shashank on 10-12-2015.
 */
'use strict';
const Hapi=require('hapi'),
    path = require('path'),
    fs = require('fs'),
    //https =  require('https'),
    //Path = require('path'),
    async = require('async'),
    //phantom = require('phantom-html2pdf'),
    config = require('./config/serverPort'),
    plugins = require('./plugin/plugins'),
    Routes =  require('./routes/routes'),
   // db = require('./dao/database'),
    configToken = require('./config/token'),
    Bootstrap = require('./Utils/Bootstrap'),
    controllers = require('./controllers/index'),
    Server = new Hapi.Server();
var bcrypt = require('bcrypt');
var crypto = require('crypto');
Server.connection({
    host:config.PortSetting.host,
    port:config.PortSetting.port,
    //tls: {
    //    key: fs.readFileSync(path.join(__dirname, 'config/private.key')),
    //    cert: fs.readFileSync(path.join(__dirname, 'config/certificate.pem'))
    //}
    routes: {
        cors: true
    }
});


Server.register(plugins.Plugin, function (err) {
    if(err){
     throw err;
    }
    Server.auth.strategy('accessToken','jwt', {
        key:configToken.secretKey ,
        validateFunc: controllers.encrypt.validate,
        verifyOptions: { 
            algorithms: [ configToken.algorithm.algorithm ] 
        }
    });
    Server.views({
        engines: {
            html: require('handlebars')
        },
        relativeTo: __dirname,
        path: './Views'
    });

    Server.start(function(){
        //let arrayData = [];
        //  async.auto({
        //      count: (cb)=> {
        //          for (let i = 0; i < 2000000; i++ ) {
        //              let sh = i
        //              arrayData.push(sh);
        //          }
        //          cb();
        //      },
        //      checkFunction:['count', (res, cb)=> {
        //          async.forEach(arrayData, (da, cb)=> {
        //              console.log(">>>here>>>",da);
        //              cb();
        //          }, (err, res)=> {
        //              if (err) {
        //                  return cb(err);
        //              } else {
        //                  console.log('loks>>',res);
        //                  cb();
        //              }
        //          })
        //      }
        //      ]
        //  }, (err,res)=> {
        //       if (err) {
        //           console.log('darta',err);
        //       }
        //      else {
        //           console.log('sdfha',res);
        //       }
        //  });
        let arr = [];
        let obj = {
            's': '',
            'a': '',
            'd': ''
        };
        for (let i = 0; i < 3; i++ ) {
            obj = Object.create(obj);
            obj['a'] = 1;
            obj['s'] =  obj['a'] + 5;
            obj['a'] = 9;
            obj['d'] = 2;
            arr.push(obj);
        }
        console.log(arr);


      console.log(bcrypt.hashSync('qwerty', 8));
        Bootstrap.connectSocket(Server);
        console.log('Server running at:', Server.info.uri);
    });
});
Server.route(
    [{
        method: 'GET',
        path: '/',
        handler: function (req, res) {
            //TODO Change for production server
            res.view('index')
        }
    },{
        method: 'GET',
        path: '/customer',
        handler: function (req, res) {
            //TODO Change for production server
            res.view('admin')
        }
    }]
);
Server.route(Routes);