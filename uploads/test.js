/**
 * Created by cl-macmini-48 on 7/23/16.
 */
"use strict";

var phantom = require('phantom-html2pdf');
var async =  require('async');
let fs = require('fs');
let Path = require('path');
let ATTACHMENT = null;
let pdfData =  "";


async.series([ function(cabk) {






            ATTACHMENT = `<table  style="width: 100%;  -webkit-print-color-adjust: exact; max-width: 700px;font-family:arial !important;margin: 0 auto;background-size:195mm 282mm !important;background: url('http://i68.tinypic.com/2dmies3.png') no-repeat center center / 100% auto !important;">` + //background-position: 0% -97% !important;
       `        <tr>` +
       `            <td>` +
       `        <table style="width: 100%">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td>` +
       `                    <h5 style="` +
       `                        font-size: 23px;font-family:arial !important;` +
       `                        padding-left: 83px;` +
       `                        margin: 5px 0;` +
       `                        color: #E6783D;` +
       `                              ">` +
       `                        Certificate #: ${565645}-${34 + 1}` +
       `                    </h5>` +
       `                </td>` +
       `                <td>` +
       `                    <img src='http://i66.tinypic.com/4qonmd.png' style="width: 200px;` +
       `                               height: 60px;` +
       `                               margin-right: 19px;"` +
       `                    >` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <table style="width: 100%; margin-top: -15px; margin-left: -20px">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td style="font-size: 18px;font-family:arial !important;` +
       `                         padding-left: 105px;` +
       `                         font-style: normal;` +
       `                         font-weight: 600;` +
       `                         color: #707173;">` +
       `                    Shippers Details:` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <h5 style="` +
       `                        font-size: 13px;font-family:arial !important;` +
       `                        float: right;` +
       `                        margin: 0;` +
       `                        color: #707173;` +
       `                        margin-bottom: 30px;` +
       `                        ">Booked By :</h5>` +
       `                </td>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <h5 style="` +
       `                     font-size: 14px;font-family:arial !important;` +
       `                     font-weight: 100;` +
       `                     text-align: left;` +
       `                     padding-right: 90px;` +
       `                     margin: 0;` +
       `                     color: #707173;` +
       `                     word-break: break-all;` +
       `                     float: left;` +
       `                     margin-left: 15px;` +
       `                     margin-bottom: 12px;` +
       `                     "` +
       `                    ><span style="text-transform: capitalize">{{CUSTOMER_NAME}}</span>  <span style="text-transform: capitalize">{{CUSTOMER_COMPANY_NAME}}</span> <br><span style="word-break: keep-all !important;">{{CUSTOMER_ADDRESS}}</span></h5>` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 40%;">` +
       `                    <h5 style="` +
       `                        font-size: 13px;font-family:arial !important;` +
       `                        float: right;` +
       `                        margin: 0;` +
       `                        color: #707173;` +
       `                        margin-bottom: 80px;` +
       `                        ">Shippers Details :</h5>` +
       `                </td>` +
       `                <td style="text-align: center;width: 60%;">` +
       `                    <h5 style="` +
       `                     font-size: 14px;font-family:arial !important;` +
       `                     font-weight: 100;` +
       `                     text-align: left;` +
       `                     padding-right: 90px;` +
       `                     margin: 0;` +
       `                     color: #707173;` +
       `                     word-break: break-all;` +
       `                     float: left;` +
       `                     margin-left: 15px;` +
       `                     margin-bottom: 60px;` +
       `                     "` +
       `                    ><span style="text-transform: capitalize">{{shipperDetails}} </span><br><span style="word-break: keep-all !important;">{{ShipperLocation}}</span></h5>` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <table style="width: 100%; margin-top: -40px; margin-left: -20px">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td style="font-size: 18px;font-family:arial !important;` +
       `                         padding-left: 108px;` +
       `                         font-style: normal;` +
       `                         font-weight: 600;` +
       `                         color: #707173;">` +
       `                    Container ID:` +
       `                </td>` +
       `                <td>` +
       `                    <h5 style="` +
       `                     font-size: 18px;font-family:arial !important;` +
       `                     float: left;` +
       `                     margin: 0;` +
       `                     color: #E6783D;` +
       `                     margin-left: -30px;` +
       `                     text-transform: uppercase;` +
       `                           ">` +
       `                        {{containerId${3333}}}` +
       `                    </h5>` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <h5 style="` +
       `                        font-size: 13px;font-family:arial !important;` +
       `                        float: right;` +
       `                        margin: 0;` +
       `                        color: #707173;` +
       `                        /*margin-bottom: 35px;*/` +
       `                        ">Maximum Gross Mass :</h5>` +
       `                </td>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <h5 style="` +
       `                     font-size: 13px;font-family:arial !important;` +
       `                     font-weight: 100;` +
       `                     text-align: right;` +
       `                     padding-right: 90px;` +
       `                     margin: 0;` +
       `                     color: #707173;` +
       `                     word-break: break-all;` +
       `                     float: left;` +
       `                     /*margin-bottom: 32px;*/` +
       `                     margin-left: 15px;` +
       `                     ">{{maxGrossWeight${113}}} kg</h5>` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <span style="margin-top: -25px;">` +
       `                        <h5 style="` +
       `                        font-size: 13px;font-family:arial !important;` +
       `                        float: right;` +
       `                        margin: 0;` +
       `                        color: #707173;` +
       `                        margin-bottom: 35px;` +
       `                        ">Container Tare Mass:</h5>` +
       `                    </span>` +
       `                </td>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <span style="margin-top: -25px;">` +
       `                        <h5 style="` +
       `                     font-size: 13px;font-family:arial !important;` +
       `                     font-weight: 100;` +
       `                     text-align: right;` +
       `                     padding-right: 90px;` +
       `                     margin: 0;` +
       `                     color: #707173;` +
       `                     word-break: break-all;` +
       `                     float: left;` +
       `                     margin-bottom: 32px;` +
       `                     margin-left: 15px;` +
       `                     ">{{containerTareMass${53}}} kg</h5>` +
       `                    </span>` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <table style="width: 100%; margin-top: -20px">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td style="text-align: center;">` +
       `                    <h3 style="` +
       `                        font-size: 18px;font-family:arial !important;` +
       `                        margin: 0 auto 15px;` +
       `                        color: #707173;` +
       `                        ">Container Verified Gross Mass :</h3>` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;">` +
       `                    <h3 style="font-size: 43px;font-family:arial !important;` +
       `                         margin: -10px auto 0;` +
       `                        color: #E6783D;">{{containerCalcWeight${33}}} kg` +
       `                        <p style="font-size: 11px;color:gray; margin-top: 10px;">Min = 1000kg  Max = 40000kg e = 50kg</p>` +
       `                    </h3>` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <table style="width: 100%; margin-top: -5px">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td style="width: 50%;">` +
       `                    <span style="font-size: 18px;font-family:arial !important;` +
       `                        float: right;` +
       `                        font-style: normal;` +
       `                        font-weight: 600;` +
       `                        color: #707173;` +
       `                        ">` +
       `                        Weight Status:` +
       `                    </span>` +
       `                </td>` +
       `                <td style="width: 50%;">` +
       `                    <img src='http://i63.tinypic.com/sli6fl.png' style="margin-left: 10px; height: 46px">` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <table style="width: 100%">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td style="width:90%">` +
       `                    <table style="width: 100%;margin-left: 60px; margin-top: 20px">` +
       `                        <tbody>` +
       `                        <tr>` +
       `                            <td style="` +
       `                              font-size: 18px;font-family:arial !important;` +
       `                            font-weight: 600;` +
       `                            color: #707173;` +
       `                            margin-left: 98px;` +
       `                            width: 270px;` +
       `                            padding: 0 0 0 25px">` +
       `                                Weighing Job Details:` +
       `                            </td>` +
       `                        </tr>` +
       `                        <tr>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                <h5 style="` +
       `                                 font-size: 13px;` +
       `                                 float: right;` +
       `                                 margin: 0;` +
       `                                 color: #707173;` +
       `                                 /*margin-bottom: 35px;*/` +
       `                                 ">Booking Number:</h5>` +
       `                            </td>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                <h5 style="` +
       `                              font-size: 13px;font-family:arial !important;` +
       `                              font-weight: 100;` +
       `                              text-align: right;` +
       `                              padding-right: 90px;` +
       `                              margin: 0;` +
       `                              color: #707173;` +
       `                              word-break: break-all;` +
       `                              float: left;` +
       `                              /*margin-bottom: 32px;*/` +
       `                              margin-left: 15px;` +
       `                              "` +
       `                                >{{bookingNumber}}</h5>` +
       `                            </td>` +
       `                        </tr>` +
       `                        <tr>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                    <span style="margin-top: -25px;">` +
       `                                        <h5 style="` +
       `                                 font-size: 13px;` +
       `                                 float: right;` +
       `                                 margin: 0;` +
       `                                 color: #707173;` +
       `                                 /*margin-bottom: 35px;*/` +
       `                                 ">Weight Seal ID:</h5>` +
       `                                    </span>` +
       `                            </td>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                    <span style="margin-top: -25px;">` +
       `                                        <h5 style="` +
       `                              font-size: 13px;font-family:arial !important;` +
       `                              font-weight: 100;` +
       `                              text-align: right;` +
       `                              padding-right: 90px;` +
       `                              margin: 0;` +
       `                              color: #707173;` +
       `                              word-break: break-all;` +
       `                              float: left;` +
       `                              /*margin-bottom: 32px;*/` +
       `                              margin-left: 15px;` +
       `                              "` +
       `                                        >{{sealId${343}}}</h5>` +
       `                                    </span>` +
       `                            </td>` +
       `                        </tr>` +
       `                        <tr>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                <div style="margin-top: -25px;">` +
       `                                    <h5 style="` +
       `                                 font-size: 13px;font-family:arial !important;` +
       `                                 float: right;` +
       `                                 margin: 0;` +
       `                                 color: #707173;` +
       `                                 padding-top: 25px;` +
       `                                 /*margin-bottom: 35px;*/` +
       `                                 ">Date & Time:</h5>` +
       `                                </div>` +
       `                            </td>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                    <span style="margin-top: -25px;">` +
       `                                        <h5 style="` +
       `                              font-size: 13px;` +
       `                              font-weight: 100;font-family:arial !important;` +
       `                              padding-right: 30px;` +
       `                              margin: 0;` +
       `                              color: #707173;` +
       `                              word-break: break-all;` +
       `                              float: left;` +
       `                              /*margin-bottom: 32px;*/` +
       `                              margin-left: 15px;` +
       `                              "` +
       `                                        >{{dateTime}}</h5>` +
       `                                    </span>` +
       `                            </td>` +
       `                        </tr>` +
       `                        <tr>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                    <span style="margin-top: -25px;">` +
       `                                        <h5 style="` +
       `                                 font-size: 13px;font-family:arial !important;` +
       `                                 float: right;` +
       `                                 margin: 0;` +
       `                                 color: #707173;` +
       `                                 /*margin-bottom: 35px;*/` +
       `                                 ">Equipment ID:</h5>` +
       `                                    </span>` +
       `                            </td>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                    <span style="margin-top: -25px;">` +
       `                                        <h5 style="` +
       `                              font-size: 13px;font-family:arial !important;` +
       `                              font-weight: 100;` +
       `                              text-align: right;` +
       `                              padding-right: 90px;` +
       `                              margin: 0;` +
       `                              color: #707173;` +
       `                              word-break: break-all;` +
       `                              float: left;` +
       `                              /*margin-bottom: 32px;*/` +
       `                              margin-left: 15px;` +
       `                              "` +
       `                                        >{{equipmentBarCodeId${21312}}}</h5>` +
       `                                    </span>` +
       `                            </td>` +
       `                        </tr>` +
       `                        <tr>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                    <span style="margin-top: -25px;">` +
       `                                        <h5 style="` +
       `                                 font-size: 13px;font-family:arial !important;` +
       `                                 float: right;` +
       `                                 margin: 0;` +
       `                                 color: #707173;` +
       `                                 margin-bottom: 35px;` +
       `                                 ">Calibration Status & Date:</h5>` +
       `                                    </span>` +
       `                            </td>` +
       `                            <td style="text-align: center;width: 50%;">` +
       `                                    <span style="margin-top: -25px;">` +
       `                                        <h5 style="` +
       `                              font-size: 13px;font-family:arial !important;` +
       `                              font-weight: 100;` +
       `                              padding-right: 25px;` +
       `                              margin: 0;` +
       `                              color: #707173;` +
       `                              word-break: break-all;` +
       `                              float: left;` +
       `                              margin-bottom: 32px;` +
       `                              margin-left: 15px;` +
       `                              "` +
       `                                >Valid - 20 December 2016</h5>` +
       `                                    </span>` +
       `                            </td>` +
       `                        </tr>` +
       `                        </tbody>` +
       `                    </table>` +
       `                </td>` +
       `                <td style="width:10%">` +
       `                    <span style="position: relative;">` +
       `                        <span style="` +
       `                        float: left;font-family:arial !important;` +
       `                        width: 100%;` +
       `                        text-align: center;` +
       `                        color: #D65627;` +
       `                        font-size: 18px;` +
       `                        ">{{top${12}}} %</span>` +
       `                        <span style="` +
       `                        top: -500%;` +
       `                        float: left;` +
       `                        position: absolute;` +
       `                        left: -25px;` +
       `                        color: #D65627;` +
       `                        font-size: 15px;` +
       `                        ">{{left${22}}} %</span>` +
       `                        <img src="http://i63.tinypic.com/dyvlsp.png" style="` +
       `                        width: 100px;` +
       `                      height: 150px;` +
       `                      margin-right: 20px; background: transparent;` +
       `                        ">` +
       `                        <span style="` +
       `                        float: left;` +
       `                        width: 100%;left:3%;` +
       `                        text-align: center;font-family:arial !important;` +
       `                        position: absolute;` +
       `                        bottom: 2px;` +
       `                        color: #D65627;` +
       `                        font-size: 18px;` +
       `                        ">{{bottom${33}}} %</span>` +
       `                        <span style="` +
       `                        position: absolute;` +
       `                        top: -500%;` +
       `                        right: -10px;` +
       `                        color: #D65627;` +
       `                        font-size: 15px;` +
       `                        ">{{right${22}}} %</span>` +
       `                    </span>` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <table style="width:100%; margin-top: -15px">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td style="width:50%;">` +
       `                    <span style="font-size: 18px;font-family:arial !important;` +
       `                         float: left;` +
       `                         font-style: normal;` +
       `                         font-weight: 600;` +
       `                         margin-left: 88px;` +
       `                         color: #707173;">` +
       `                        Weight Certified By:` +
       `                    </span>` +
       `                </td>` +
       `                <td style="width:50%;">` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="width:50%;text-align: center;">` +
       `                   <img src="{{signature}}" style="max-width: 200px;max-height: 60px;">` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="width:51%;">` +
       `                    <p style="font-size: 16px;font-family:arial !important;` +
       `                   font-style: normal;` +
       `                   font-weight: 600;` +
       `                   color: #707173;` +
       `                   margin-left: 90px;` +
       `                   margin-top: -5px;` +
       `                     ">{{CONTRACTOR_NAME}}</p>` +
       `                    <p style="font-size: 13px;` +
       `                font-style: normal;` +
       `                font-weight: 200;` +
       `                color: #707173;` +
       `                margin-top: -20px;` +
       `                margin-left: 88px;` +
       `                margin-bottom: 2px;` +
       `                  ">Conweigh Accredited Technician</p>` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <span style="margin-top: -25px;">` +
       `                        <h5 style="` +
       `                        font-size: 13px;font-family:arial !important;` +
       `                        float: right;` +
       `                        margin: 10px 0 0 10px;` +
       `                        color: #707173;` +
       `                        /*margin-bottom: 35px;*/` +
       `                        ">Technician Registration Number:</h5>` +
       `                    </span>` +
       `                </td>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <span style="margin-top: -25px;">` +
       `                        <h5 style="` +
       `                     font-size: 13px;font-family:arial !important;` +
       `                     font-weight: 100;` +
       `                     text-align: right;` +
       `                     padding-right: 90px;` +
       `                     margin: 10px 0 0 0;` +
       `                     color: #707173;` +
       `                     word-break: break-all;` +
       `                     float: left;` +
       `                     /*margin-bottom: 32px;*/` +
       `                     margin-left: 15px;` +
       `                     "` +
       `                        >CWT{{contractorId}}</h5>` +
       `                    </span>` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <span style="margin-top: -40px;">` +
       `                        <h5 style="` +
       `                        font-size: 13px;font-family:arial !important;` +
       `                        float: right;` +
       `                        margin: 0;` +
       `                        color: #707173;` +
       `                        margin-bottom: 35px;` +
       `                        " ng-show="!certifiedName">{{certifiedName}}</h5>` +
       `                    </span>` +
       `                </td>` +
       `                <td style="text-align: center;width: 50%;">` +
       `                    <span style="margin-top: -40px;">` +
       `                        <h5 style="` +
       `                     font-size: 13px;font-family:arial !important;` +
       `                     font-weight: 100;` +
       `                     text-align: right;` +
       `                     padding-right: 90px;` +
       `                     margin: 0;` +
       `                     color: #707173;` +
       `                     word-break: break-all;` +
       `                     float: left;` +
       `                     margin-bottom: 32px;` +
       `                     margin-left: 15px;` +
       `                     "` +
       `                            ng-show="!certifiedName">{{contractorCompany}}</h5>` +
       `                    </span>` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <table style="width:100%">` +
       `            <tbody>` +
       `            <tr>` +
       `                <td style="text-align: center;" colspan="2">` +
       `                    <span style="` +
       `                     font-size: 14px;font-family:arial !important;` +
       `                   float: left;` +
       `                   font-style: normal;` +
       `                   font-weight: 600;` +
       `                   color: #707173;` +
       `                   margin-left: 66px;` +
       `                   margin-top: 10px;` +
       `                     ">` +
       `                        Conweigh Pty. Ltd.` +
       `                    </span>` +
       `                    <span style="` +
       `                     font-size: 10px;font-family:arial !important;` +
       `                   float: left;` +
       `                   font-style: normal;` +
       `                   font-weight: 600;` +
       `                   color: #707173;` +
          //`                   margin-left: 65px;`+
       `                   margin-top: 10px;` +
       `                     ">(ABN 67 609 108 796)` +
       `                    </span>` +
       `                </td>` +
          //`                <td style="text-align: center;width: 50%;"></td>`+
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 40%;">` +
       `                    <span style="` +
       `                     font-size: 10px;` +
       `                     font-style: normal;font-family:arial !important;` +
       `                     font-weight: 600;width:100%;` +
       `                     color: #707173;` +
       `                     padding-left: 68px;` +
       `                     margin-top: 0px;` +
       `                     ">1/58 Frederick St. Northgate QLD ` +
       `                    </span><br><span style="` +
       `                     font-size: 10px;` +
       `                     font-style: normal;font-family:arial !important;` +
       `                     font-weight: 600;width:100%;` +
       `                     color: #707173;` +
       `                     padding-left: 68px;` +
       `                     margin-top: 0px;` +
       `                     " >Australia 4013</span>` +
       `                </td>` +
       `                <td style="text-align: center;width: 60%;">` +
       `                    <span style="` +
       `                     font-size: 13px;font-family:arial !important;` +
       `                   font-style: normal;` +
       `                   font-weight: 600;` +
       `                   color: #707173;` +
       `                   margin-left: 140px;` +
       `                   margin-top: 0px;` +
       `                     ">Phone: 1300 022 232` +
       `                    </span>` +
       `                </td>` +
       `            </tr>` +
       `            <tr>` +
       `                <td style="text-align: center;width: 40%;">` +
       `                    <span style="` +
       `                     font-size: 10px;` +
       `                     font-style: normal;font-family:arial !important;` +
       `                     font-weight: 600;` +
       `                     color: #707173;` +
       `                     margin-left: 37px;` +
       `                     margin-top: 0px;` +
       `                     ">www.conweigh.net` +
       `                    </span>` +
       `                </td>` +
       `                <td style="text-align: center;width: 60%;">` +
       `                    <span style="` +
       `                     font-size: 13px;font-family:arial !important;` +
       `                   font-style: normal;` +
       `                   font-weight: 600;` +
       `                   color: #707173;` +
       `                   margin-left: 115px;` +
       `                   margin-top: 0px;` +
       `                     ">Email: vgm@conweigh.net` +
       `                    </span>` +
       `                </td>` +
       `            </tr>` +
       `            </tbody>` +
       `        </table>` +
       `        <span style="height: 20px;background-color: #FFF;"></span>` +
       `        </td>` +
       `        </tr>` +
       `    </table>`;
    cabk();
}, function (cabk) {
   let Handlebars = require('handlebars');
   let data = {};
   data.contractorCompany  = "Rocks";
   pdfData = Handlebars.compile(ATTACHMENT)(data.contractorCompany);
   cabk();
}], function (err, res) {
   let md5 = require('md5');
   let CurrentDate = new Date().toISOString();
   CurrentDate += Math.ceil(Math.random() * 20000);
   CurrentDate = md5(CurrentDate);
   let HtmlFilePath = Path.resolve(".") + "/" + "InvoiceVGM" + CurrentDate + ".html";
   let pdfPath = Path.resolve(".") + "/" + "Invoice2VGM" + CurrentDate + ".pdf";
   fs.writeFile(HtmlFilePath, pdfData, function (err) {
      if (err) {
         console.log(err);
      }
      else {
         //phantom.create().then(function(ph) {
         //    ph.createPage().then(function(page) {
         //        page.viewportSize = { width: 1440, height: 2036 };
         //        //page.zoomFactor = 0.53;
         //        //page.paperSize = {  format: 'A4',  orientation: 'portrait', margin: '0cm' };
         //        page.open(HtmlFilePath).then(function(status) {
         //            var height = page.evaluate(function() {
         //                    return parseInt(document.documentElement.scrollHeight)
         //                }),
         //                width = page.evaluate(function() {
         //                    return document.documentElement.scrollWidth
         //                });
         //
         //            console.log("Status: " + status);
         //            height += 28.8;
         //            page.paperSize = {
         //                width: width,
         //                height: height,
         //                margin: {
         //                    top: '0.5in',
         //                    bottom: '0.2in',
         //                    left: '0.2in',
         //                    right: '0.2in'
         //                },
         //            };
         //           let style = 'background:url("http://tinypic.com/r/sb0u2x/9") no-repeat;';
         //            style += 'background-size: contain;';
         //            style += 'width: 100%; height: 100%;';
         //            page.content = '<html><body style="' + style + '"></body></html>';
         //            page.render(pdfPath).then(function() {
         //                console.log('Page Rendered');
         //                ph.exit();
         //                return cb(null, 1);
         //            });
         //        });
         //    });
         //});
         //let options = {
         //   "html": HtmlFilePath,
         //   "paperSize": {
         //       format: 'A4', orientation: 'portrait', border: '0cm', width: 900, height: 1100
         //   }
         //   //"deleteOnAction" : true
         //};
         //
             var pdf = require('html-pdf');
             //var options = { format: 'letter' };
             var html = fs.readFileSync(HtmlFilePath, 'utf8');
             pdf.create(html).toFile(pdfPath, function(err, res) {
                    if (err) return console.log(err);
                    console.log(res); // { filename: '/app/businesscard.pdf' }
             });

            //phantom.convert(options, function (err, result) {
            //
            //   console.log("here result of phantom", result);
            //   /* Using a buffer and callback */
            //   result.toBuffer(function (returnedBuffer) {
            //   });
            //
            //   /* Using a readable stream */
            //   const stream = result.toStream();
            //
            //   /* Using the temp file path */
            //   const tmpPath = result.getTmpPath();
            //
            //   /* Using the file writer and callback */
            //   result.toFile(pdfPath, function () {
            //      console.log('Page Rendered');
            //      //ph.exit();
            //      console.log("done");
            //   });
            //});

      }
   });
});

