/**
 * Created by Shashank on 23-12-2015.
 */
var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema,
    UserSchema = new Schema({
        "Id": { type: Schema.ObjectId, required: true , ref:"User"},
        "follower":{type: Number, default: 0, ref:"User" },
        "follows":{type: Number, default: 0, ref:"User" },
        "followerNames":{type: Array, ref:"User"  },
        "followingNames":{type: Array, ref:"User" }
    });
module.exports = Mongoose.model('UserFollower', UserSchema);