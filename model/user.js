/**
 * Created by Shashank on 14-12-2015.
 */
var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema,
    UserSchema = new Schema({
        "firstName": { type: String,required:true},
        "lastName":{type:String,required:true},
        "email":{type:String,unique:true,required:true},
        "password":{type:String,required:true},
        "updatedAt":{type:Date},
        "createdAt":{type:Date},
        "accessToken":{type:String},
        "visits":{type: Number, default: 0},
        "followTable": { type: Schema.ObjectId, ref: 'UserFollower' }
});
UserSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();
    // change the updated_at field to current date
    this.updatedAt = currentDate;
    // if created_at doesn't exist, add to that field
    if (!this.createdAt)
        this.createdAt = currentDate;
    next();
});
module.exports=Mongoose.model('User', UserSchema);