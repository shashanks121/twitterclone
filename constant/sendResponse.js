/**
 * Created by Shashank on 20-12-2015.
 */
'use strict';
const response=require('./response');
exports.invalidInput=function(reply){
send(response.standardHTTPErrors[2],reply);
};
exports.emailExistError=function(reply){
  send(response.EmailErrorMessage,reply);
};
exports.somethingWentWrongError = function(reply){
    send(response.standardHTTPErrors[3],reply);
};
exports.parameterMissingError = function (reply){
    send(response.standardHTTPErrors[2],reply);
};
exports.successMsgToken=function(token,reply){
    response.success[0].accessToken = token;
    send( response.success[0], reply);
};
exports.successData=function(data,reply){
    response.success[0].message.Data=data;
    send( response.success[0], reply);
}
function send(data,reply){
    if(data.hasOwnProperty('accessToken'))
    return reply(data.message).code(data.code).header("Authorization", data.accessToken);
    else{
        reply(data.message).code(data.code)
    }
};