/**
 * Created by Shashank on 11-12-2015.
 */
'use strict';
var Inert = require('inert'),
    Vision = require('vision'),
    HapiSwagger = require('hapi-swagger'),
    Pack = require('../package'),
    HapiAuth = require('hapi-auth-jwt');
var Plugin=[Inert, Vision,HapiAuth,{
    register: HapiSwagger,
    options:  {
        apiVersion: Pack.version,
        pathPrefixSize:3
    }
}
];
module.exports={Plugin};